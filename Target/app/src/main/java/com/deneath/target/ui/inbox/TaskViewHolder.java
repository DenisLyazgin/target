package com.deneath.target.ui.inbox;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.deneath.target.R;
import com.deneath.target.model.Task;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 02.09.2017.
 */

public class TaskViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title_text_view) TextView mTitleTextView;
    @BindView(R.id.description_text_view) TextView mDescriptionTextView;
    @BindView(R.id.priority_label_text_view) TextView mPriorityLabelTextView;
    @BindView(R.id.priority_text_view) TextView mPriorityTextView;
    @BindView(R.id.type_label_text_view) TextView mTypeLabelTextView;
    @BindView(R.id.type_text_view) TextView mTypeTextView;
    @BindView(R.id.priority_indicator) View mPriorityIndicator;
    @BindView(R.id.containerLayout) View mHostLayout;

    TaskViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    void bind(Task task, Listener listener, View.OnLongClickListener longClickListener) {
        mTitleTextView.setText(task.getTitle());
        mDescriptionTextView.setText(task.getDescription());
        mPriorityLabelTextView.setText(itemView.getContext().getResources().getString(R.string.label_priority));
        mPriorityTextView.setText(task.getPriority().toString());
        mTypeLabelTextView.setText(itemView.getContext().getResources().getString(R.string.label_type));
        mTypeTextView.setText(task.getType().toString());
        mPriorityIndicator.setBackgroundColor(task.getPriority().toColor());
        mHostLayout.setOnDragListener(new DragListener(listener));
        mHostLayout.setTag(getAdapterPosition());
        mHostLayout.setOnLongClickListener(longClickListener);

    }
}
