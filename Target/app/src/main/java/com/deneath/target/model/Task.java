package com.deneath.target.model;

import android.support.v4.content.ContextCompat;

import com.deneath.target.R;
import com.deneath.target.dagger.TargetApplication;

/**
 * Created by myduc on 02.09.2017.
 */

public class Task {


    public enum Priority {
        Critical, Immediate, Normal, Minor;

        @Override
        public String toString() {
            switch (this) {
                case Critical:
                    return TargetApplication.getContext().getString(R.string.task_priority_critical);
                case Immediate:
                    return TargetApplication.getContext().getString(R.string.task_priority_immediate);
                case Normal:
                    return TargetApplication.getContext().getString(R.string.task_priority_normal);
                case Minor:
                    return TargetApplication.getContext().getString(R.string.task_priority_minor);
                default:
                    throw new IllegalArgumentException(this.name());
            }
        }

        public int toColor() {
            switch (this) {
                case Critical:
                    return ContextCompat.getColor(TargetApplication.getContext(), R.color.priority_red);
                case Immediate:
                    return ContextCompat.getColor(TargetApplication.getContext(), R.color.priority_yellow);
                case Normal:
                    return ContextCompat.getColor(TargetApplication.getContext(), R.color.priority_green);
                case Minor:
                    return ContextCompat.getColor(TargetApplication.getContext(), R.color.priority_blue);
                default:
                    throw new IllegalArgumentException(this.name());
            }
        }
    }

    public enum State {
        Open, InProgress, Completed
    }

    public enum Type {
        Health, Work, Learning, Family;

        @Override
        public String toString() {
            switch (this) {
                case Health:
                    return TargetApplication.getContext().getString(R.string.task_type_health);
                case Work:
                    return TargetApplication.getContext().getString(R.string.task_type_work);
                case Learning:
                    return TargetApplication.getContext().getString(R.string.task_type_learning);
                case Family:
                    return TargetApplication.getContext().getString(R.string.task_type_family);
                default:
                    throw new IllegalArgumentException(this.name());
            }
        }
    }

    private String Title;
    private String Description;
    private State State;
    private Type Type;
    private Priority Priority;

    public Task(String title, String description, Task.State state, Task.Type type, Task.Priority priority) {
        Title = title;
        Description = description;
        State = state;
        Type = type;
        Priority = priority;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Task.State getState() {
        return State;
    }

    public void setState(Task.State state) {
        State = state;
    }

    public Task.Type getType() {
        return Type;
    }

    public void setType(Task.Type type) {
        Type = type;
    }

    public Task.Priority getPriority() {
        return Priority;
    }

    public void setPriority(Task.Priority priority) {
        Priority = priority;
    }
}
