package com.deneath.target.ui.main;

import dagger.Module;
import dagger.Provides;

/**
 * Created by myduc on 01.09.2017.
 */
@Module
public class MainModule {
    @Provides MainPresenter providePresenter(){
        return new MainPresenter();
    }
}
