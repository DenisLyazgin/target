package com.deneath.target.ui.inbox;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.deneath.target.R;
import com.deneath.target.dagger.TargetApplication;
import com.deneath.target.model.Task;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by myduc on 01.09.2017.
 */

public class InboxActivity extends AppCompatActivity implements IInboxView, Listener {

    @Inject
    InboxPresenter mPresenter;
    @Inject
    ListAdapter mOpenAdapter;
    @Inject
    ListAdapter mInProgressAdapter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.rvTop)
    RecyclerView rvTop;
    @BindView(R.id.rvBottom)
    RecyclerView rvBottom;
    @BindView(R.id.tvEmptyListTop)
    TextView tvEmptyListTop;
    @BindView(R.id.tvEmptyListBottom)
    TextView tvEmptyListBottom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        TargetApplication.getApplication(this).getInboxComponent().inject(this);
        ButterKnife.bind(this);

        mPresenter.setView(this);
        mPresenter.OnCreate();
    }

    @Override
    public void initViews() {
        initTopRecyclerView();
        initBottomRecyclerView();

        tvEmptyListTop.setVisibility(View.GONE);
        tvEmptyListBottom.setVisibility(View.GONE);

//todo
        mToolbar.setTitle("Inbox");
        setSupportActionBar(mToolbar);

        tvEmptyListTop.setOnDragListener(mOpenAdapter.getDragInstance());
        rvTop.setOnDragListener(mOpenAdapter.getDragInstance());
        tvEmptyListBottom.setOnDragListener(mInProgressAdapter.getDragInstance());
        rvBottom.setOnDragListener(mInProgressAdapter.getDragInstance());
    }

    @Override
    public void showList(ArrayList<Task> list) {

    }

    private void initTopRecyclerView() {
        rvTop.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));

        List<Task> topList = new ArrayList<>();
        topList.add(new Task("sdfsdf", "sdfasdfasdfasdfsadfsadf", Task.State.Completed, Task.Type.Family, Task.Priority.Immediate));

        mOpenAdapter.setSource(topList);
        mOpenAdapter.setListener(this);
        rvTop.setAdapter(mOpenAdapter);
    }

    private void initBottomRecyclerView() {
        rvBottom.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));

        List<Task> bottomList = new ArrayList<>();
        bottomList.add(new Task("sdfsdf", "sdfasdfasdfasdfsadfsadf", Task.State.Completed, Task.Type.Family, Task.Priority.Critical));

        mInProgressAdapter.setSource(bottomList);
        mInProgressAdapter.setListener(this);
        rvBottom.setAdapter(mInProgressAdapter);
    }

    @Override
    public void setEmptyListTop(boolean visibility) {
        tvEmptyListTop.setVisibility(visibility ? View.VISIBLE : View.GONE);
        rvTop.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setEmptyListBottom(boolean visibility) {
        tvEmptyListBottom.setVisibility(visibility ? View.VISIBLE : View.GONE);
        rvBottom.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }
}
