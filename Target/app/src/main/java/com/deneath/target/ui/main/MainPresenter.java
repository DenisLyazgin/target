package com.deneath.target.ui.main;

import com.deneath.target.infrastructure.BasePresenter;

/**
 * Created by myduc on 01.09.2017.
 */

public class MainPresenter extends BasePresenter<IMainView> {

    public void onCreate(){
        mView.launchInboxScreen();
    }

}
