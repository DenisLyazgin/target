package com.deneath.target.ui.inbox;

import dagger.Module;
import dagger.Provides;

/**
 * Created by myduc on 01.09.2017.
 */
@Module
public class InboxModule {

    @Provides InboxPresenter providePresenter(){
        return new InboxPresenter();
    }

    @Provides ListAdapter provideAdapter(){
        return new ListAdapter();
    }
}
