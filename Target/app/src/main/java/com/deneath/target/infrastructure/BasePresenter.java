package com.deneath.target.infrastructure;

/**
 * Created by myduc on 01.09.2017.
 */

public abstract class BasePresenter<T> {

    protected T mView;

    public void setView(T view){
        mView = view;
    }
}
