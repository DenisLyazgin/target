package com.deneath.target.infrastructure;

import android.support.v7.widget.RecyclerView;

import com.deneath.target.model.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myduc on 02.09.2017.
 */

public abstract class BaseRecyclerViewAdapter<T extends RecyclerView.ViewHolder, S> extends RecyclerView.Adapter<T>{

    protected List<S> mSource;

    public void setSource(List<S> source){
        mSource = source;
    }
}
