package com.deneath.target.ui.inbox;

import com.deneath.target.model.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myduc on 01.09.2017.
 */

public interface IInboxView {
    void initViews();
    void showList(ArrayList<Task> list);
}
