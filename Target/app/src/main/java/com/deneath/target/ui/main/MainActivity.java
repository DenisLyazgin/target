package com.deneath.target.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.deneath.target.R;
import com.deneath.target.dagger.TargetApplication;
import com.deneath.target.ui.inbox.InboxActivity;

import javax.inject.Inject;


public class MainActivity extends AppCompatActivity implements IMainView{

    @Inject
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TargetApplication.getApplication(this).getMainComponent().inject(this);
        presenter.setView(this);

        presenter.onCreate();
    }

    @Override
    public void launchInboxScreen() {
        startActivity(new Intent(this, InboxActivity.class));
    }
}
