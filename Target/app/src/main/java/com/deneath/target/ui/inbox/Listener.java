package com.deneath.target.ui.inbox;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 07.09.2017.
 */

interface Listener {
    void setEmptyListTop(boolean visibility);

    void setEmptyListBottom(boolean visibility);
}