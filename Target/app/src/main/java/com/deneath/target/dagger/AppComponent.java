package com.deneath.target.dagger;

import com.deneath.target.ui.main.MainActivity;
import com.deneath.target.ui.main.MainModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by myduc on 01.09.2017.
 */
@Singleton
@Component(modules = {AppModule.class, MainModule.class})
public interface AppComponent {
    void inject(MainActivity activity);
}
