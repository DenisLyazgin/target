package com.deneath.target.ui.inbox;

import dagger.Component;

/**
 * Created by myduc on 01.09.2017.
 */
@Component(modules = {InboxModule.class})
public interface IInboxComponent {
    void inject(InboxActivity activity);
    void inject(InboxPresenter presenter);
}
