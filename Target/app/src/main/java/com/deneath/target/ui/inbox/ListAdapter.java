package com.deneath.target.ui.inbox;

import android.content.ClipData;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.deneath.target.R;
import com.deneath.target.infrastructure.BaseRecyclerViewAdapter;
import com.deneath.target.model.Task;

import java.util.List;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 09.09.2017.
 */

class ListAdapter extends BaseRecyclerViewAdapter<TaskViewHolder, Task>
        implements View.OnLongClickListener {

    private Listener mListener;

    ListAdapter() {
    }

    void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.view_holder_task, parent, false);
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        holder.bind(mSource.get(position), mListener, this);
    }

    @Override
    public int getItemCount() {
        return mSource.size();
    }

    List<Task> getList() {
        return mSource;
    }

    void updateList(List<Task> list) {
        mSource = list;
    }

    DragListener getDragInstance() {
        if (mListener != null) {
            return new DragListener(mListener);
        } else {
            Log.e("ListAdapter", "Listener wasn't initialized!");
            return null;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        ClipData data = ClipData.newPlainText("", "");
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            v.startDragAndDrop(data, shadowBuilder, v, 0);
        } else {
            v.startDrag(data, shadowBuilder, v, 0);
        }
        return true;
    }
}
