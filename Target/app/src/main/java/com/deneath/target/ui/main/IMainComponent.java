package com.deneath.target.ui.main;

import dagger.Component;

/**
 * Created by myduc on 01.09.2017.
 */
@Component(modules = {MainModule.class})
public interface IMainComponent {
    void inject(MainActivity mainActivity);
}
