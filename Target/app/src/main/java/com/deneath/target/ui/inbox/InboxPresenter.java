package com.deneath.target.ui.inbox;

import com.deneath.target.infrastructure.BasePresenter;
import com.deneath.target.model.Task;

import java.util.ArrayList;

/**
 * Created by myduc on 01.09.2017.
 */

public class InboxPresenter extends BasePresenter<IInboxView> {

    private ArrayList<Task> mSource;

    public void OnCreate() {
        //get data
        mView.initViews();
        getData();
        mView.showList(mSource);
    }

    private void getData() {
        mSource = new ArrayList<>();
        mSource.add(new Task("sdfsdf", "sdfasdfasdfasdfsadfsadf", Task.State.Completed, Task.Type.Family, Task.Priority.Immediate));
        mSource.add(new Task("sdfsdf", "sdfasdfasdfasdfsadfsadf", Task.State.Completed, Task.Type.Family, Task.Priority.Minor));
        mSource.add(new Task("sdfsdf", "sdfasdfasdfasdfsadfsadf", Task.State.Completed, Task.Type.Family, Task.Priority.Critical));
        mSource.add(new Task("sdfsdf", "sdfasdfasdfasdfsadfsadf", Task.State.Completed, Task.Type.Family, Task.Priority.Normal));
        mSource.add(new Task("sdfsdf", "sdfasdfasdfasdfsadfsadf", Task.State.Completed, Task.Type.Family, Task.Priority.Critical));
        mSource.add(new Task("sdfsdf", "sdfasdfasdfasdfsadfsadf", Task.State.Completed, Task.Type.Family, Task.Priority.Critical));
    }

}
