package com.deneath.target.dagger;

import android.app.Application;
import android.content.Context;

import com.deneath.target.ui.inbox.DaggerIInboxComponent;
import com.deneath.target.ui.inbox.IInboxComponent;
import com.deneath.target.ui.main.DaggerIMainComponent;
import com.deneath.target.ui.main.IMainComponent;


/**
 * Created by myduc on 01.09.2017.
 */

public class TargetApplication extends Application {

    private static Context mContext;
    private AppComponent appComponent;
    private IInboxComponent mInboxComponent;
    private IMainComponent mMainComponent;

    public static TargetApplication getApplication(Context context){

        return (TargetApplication) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public IInboxComponent getInboxComponent(){
        return mInboxComponent;
    }

    public IMainComponent getMainComponent() {
        return mMainComponent;
    }

    public static Context getContext(){
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        appComponent = DaggerAppComponent.create();
        mInboxComponent = DaggerIInboxComponent.create();
        mMainComponent = DaggerIMainComponent.create();
    }
}
